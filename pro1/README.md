# kicad_ci_test

Test for Continuous Integration using KiCad.

**Important** 2020-02-22 only works using current KiAuto/KiBot from git, not the releases.

This is an example of how to test and generate documentation for a KiCad project automatically every time you commit a change to the schematic and/or PCB.

In this example we use a [docker image](https://github.com/INTI-CMNB/kicad_auto) containing KiCad, [KiPlot](https://github.com/INTI-CMNB/kiplot) and all the supporting tools.

The [kicad_ci_test.kiplot.yaml](https://gitlab.com/set-soft/kicad-ci-test/-/blob/master/kicad_ci_test.kiplot.yaml) configures KiPlot and says what's available.

The [.gitlab-ci.yml](https://gitlab.com/set-soft/kicad-ci-test/-/blob/master/.gitlab-ci.yml) configures what GitLab makes.

In this file we:

* Instruct GitLab which stages are available:

```
stages:
  - run_erc
  - run_drc
  - gen_fab
```

* Then we define four actions and to which stage they belong. In our example *erc* belongs to *run_erc*, *drc* to *run_drc* and *sch_outputs* plus *pcb_outputs* belongs to *gen_fab*.
* Each action takes the repo content as input automatically
* The actions are executed by KiPlot and stored in the *Fabrication* directory
* The resulting files are stored as *artifacts* using:

```
  artifacts:
    paths:
      - Fabrication/
```

* We also define some dependencies to avoid running tasks for files that didn't change.

You can add a link to browse the latest *artifacts* using this URL


```
https://example.com/<namespace>/<project>/-/jobs/artifacts/<ref>/browse?job=<job_name>
```
or even have a direct download link


```
https://example.com/<namespace>/<project>/-/jobs/artifacts/<ref>/download?job=<job_name>
```

For this repo:


- [sch_outputs](https://gitlab.com/ville-klar/kicad-ci-test/-/jobs/artifacts/master/browse?job=sch_outputs)
- [pcb_outputs](https://gitlab.com/ville-klar/kicad-ci-test/-/jobs/artifacts/master/browse?job=pcb_outputs)
- [erc](https://gitlab.com/ville-klar/kicad-ci-test/-/jobs/artifacts/master/browse?job=erc)


For more information about the GitLab configuration read the [GitLab CI/CD Pipeline Configuration Reference](https://docs.gitlab.com/ee/ci/yaml/) page.

